#!/bin/bash
node index.js --input in/parts.csv --output out/parts --width 250 --height 350 --padding 10 --bkg-color '#eeeeee'
node index.js --input in/actions.csv --output out/actions --width 250 --height 350 --padding 10 --bkg-color '#ffffcc'
node index.js --input in/resources.csv --output out/resources --width 250 --height 350 --padding 10 --bkg-color '#ccffcc'


